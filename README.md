Termux golang builder
=====================

This is a docker image similar to the normal [golang](https://hub.docker.com/_/golang), except that it contains the same patches as golang does on termux.
This will therefore allow you to easily cross compile golang programs towards termux, which would make things like networking actually work.
This image is primarly meant for CI setups, where it often isn't very practical to recompile software on your phone.