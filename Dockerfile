FROM golang:latest AS builder

WORKDIR /root

RUN apt-get update && apt-get install -y build-essential clang

ENV GOLANG_VERSION="go1.19.5"

RUN wget https://storage.googleapis.com/golang/${GOLANG_VERSION}.src.tar.gz && \
    tar -xf *.tar.gz && \
    rm *.tar.gz

RUN mkdir -p /data/data/com.termux/files/usr/tmp

WORKDIR /root/go/

COPY *.patch /root/go/

RUN patch -p0 < src-os-file_unix.go.patch && \
    patch -p0 < src-runtime-cgo-cgo.go.patch && \
    patch -p0 < src-runtime-cgo-gcc_android.c.patch && \
    patch -p0 < src-resolve-conf.go.patch

WORKDIR /root/go/src/

RUN env CC_FOR_TARGET=clang \
        CXX_FOR_TARGET=clang++ \
        CC=gcc \
        GO_LDFLAGS="-extldflags=-pie" \
        GOROOT_BOOTSTRAP=$GOROOT \
        GOROOT_FINAL=/data/data/com.termux/files/usr/lib/go \
        PKG_CONFIG= \
        ./make.bash

FROM buildpack-deps:scm

RUN mkdir -p /data/data/com.termux/files/usr/tmp /data/data/com.termux/files/usr/etc
RUN ln -s /etc/resolv.conf /data/data/com.termux/files/usr/etc/resolv.conf
COPY --from=builder /root/go /data/data/com.termux/files/usr/lib/go

ENV GOPATH /go
ENV PATH=$GOPATH/bin:/data/data/com.termux/files/usr/lib/go/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin