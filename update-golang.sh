#!/bin/bash

set -euo pipefail

if [ -z "$GITHUB_TOKEN" ] || [ -z "$GITLAB_TOKEN" ] || [ -z "$CI_PROJECT_ID" ]
then
    echo "Missing either GITHUB_TOKEN, GITLAB_TOKEN or CI_PROJECT_ID "
    exit 1
fi

# We're first doing the following qraphql query to figure out the last release
# query {
#   repository(owner: "golang", name: "go") {
#     refs(refPrefix: "refs/tags/", last: 1, orderBy: {field: TAG_COMMIT_DATE, direction: ASC}) {
#       edges {
#         node {
#           name
#         }
#       }
#     }
#   }
# }

graphql_out=$(curl -X POST \
                   -H "Authorization: bearer ${GITHUB_TOKEN}" \
                -H "Content-Type: application/json" \
                --data '{"query": "query { repository(owner: \"golang\", name: \"go\") {refs(refPrefix: \"refs/tags/\", last: 1, orderBy: {field: TAG_COMMIT_DATE, direction: ASC}) {edges {node {name}}}}}"}' \
                https://api.github.com/graphql)

version=$(echo "$graphql_out" | jq '.data.repository.refs.edges[0].node.name')

if [[ $version == *"beta"* ]] || [[ $version == *"rc"* ]]; then
    exit 0
fi

if grep "ENV GOLANG_VERSION=$version" Dockerfile; then
    exit 0
fi

new_dockerfile=$(sed --regexp-extended "s/ENV GOLANG_VERSION=.+/ENV GOLANG_VERSION=$version/" < Dockerfile)

curl -X POST \
     -H "PRIVATE-TOKEN: $GITLAB_TOKEN" \
     -F "branch=update-to-$version" \
     -F "commit_message=Updated golang to $version" \
     -F "start_branch=master" \
     -F "actions[][action]=update" \
     -F "actions[][file_path]=Dockerfile" \
     -F "actions[][content]=$new_dockerfile" \
     "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/repository/commits"

curl -X POST \
     -H "PRIVATE-TOKEN: $GITLAB_TOKEN" \
     -F "source_branch=update-to-$version" \
     -F "target_branch=master" \
     -F "remove_source_branch=true" \
     -F "title=Updated golang to $version" \
     "https://gitlab.com/api/v4/projects/$CI_PROJECT_ID/merge_requests"
